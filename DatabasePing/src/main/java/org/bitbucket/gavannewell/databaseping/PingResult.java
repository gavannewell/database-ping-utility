package org.bitbucket.gavannewell.databaseping;

import java.sql.SQLException;

/**
 *
 * @author Gavan Newell
 */
public class PingResult {
    private final String activity;
    private final int repeat;
    private final boolean success;
    private final long durationMilliseconds;
    private final SQLException exception;

    public PingResult(String activity, int repeat, long durationMilliseconds) {
        this.activity = activity;
        this.repeat = repeat;
        this.success = true;
        this.durationMilliseconds = durationMilliseconds;
        this.exception = null;
    }
    
    public PingResult(String activity, int repeat, SQLException exception) {
        this.activity = activity;
        this.success = false;
        this.repeat = repeat;
        this.durationMilliseconds = -1;
        this.exception = exception;
    }

    public String getActivity() {
        return activity;
    }

    public boolean isSuccess() {
        return success;
    }

    public long getDurationMilliseconds() {
        return durationMilliseconds;
    }

    public SQLException getException() {
        return exception;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(activity);
        sb.append(',');
        sb.append(repeat);
        sb.append(',');
        sb.append(durationMilliseconds);
        sb.append(',');
        if( this.exception == null )
        {
            sb.append("OK");
        }
        else
        {
            sb.append("Code=").append(this.exception.getErrorCode());
            sb.append(";State=").append(stripNewlines(this.exception.getSQLState()));
            sb.append(";Error=").append(stripNewlines(this.exception.getMessage()));
        }
        sb.append('\n');
        return sb.toString();
    }
    
    private static String stripNewlines(String input)
    {
        if( input == null ) return "";
        return input.replace("\n", " ").replace(",", ";").replace("\r", "").replace("\t", " ");
    }
}
