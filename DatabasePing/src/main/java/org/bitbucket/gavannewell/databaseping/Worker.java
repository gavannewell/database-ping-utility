package org.bitbucket.gavannewell.databaseping;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.SwingWorker;


/**
 *
 * @author Gavan Newell
 */
public class Worker extends SwingWorker<String,Integer> {

    private final MainWindow callback;
//    private final List<PingResult> results = new ArrayList<PingResult>();
    private final DatabaseType type;
    private final String host;
    private final String database;
    private final String username;
    private final String password;
    private Connection conn = null;
    private String previousConnectionId = "";
    private String currentConnectionId = "";
    private StringBuilder results = new StringBuilder();
    private long bandwidth = 0;
    private int testNumber = 0;
    private int connection = 0;

    public Worker(MainWindow callback, DatabaseType type, String host, String database, String username, String password) {
        this.callback = callback;
        this.type = type;
        this.host = host;
        this.database = database;
        this.username = username;
        this.password = password;
    }

    
    /**
     * Establish a connection to the database. Track the connection ID
     * so we can detect any drop/reconnect that the driver does behind the
     * scene. Keep this open for the duration of the test. Every 5 seconds
     * perform a ping (SELECT 1) query. After 10 of these perform a bandwidth
     * test (SELECT 1M) query. Then repeat the cycle. Re-establish connection
     * to database if lost before each attempt. Record timestamps as well as
     * data fields in the results in CSV friendly format for later analysis.
     */
    @Override
    protected String doInBackground() throws Exception {
        
        loadDrivers();
        
        // Add headers to answer
        this.results.append("TestNumber,Timestamp,Connection,Ping(ms),Bandwidth(ms/MB)\n");
        this.publish(0);
        
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date now = new Date();
        
        while( !this.isCancelled() )
        {
            final long startTimestamp = System.currentTimeMillis();
            now.setTime(startTimestamp);
            
            // (Re)connect if required and update the connection identifier.
            // Record time taken to execute this step.
            long pingTimeMillis = fetchConnectionAndIdentifier();
            
            // Compare current connection identifier with previous and increment
            // counter if different. Also increment if connection can't be
            // established.
            if( this.conn == null || !this.previousConnectionId.equals(this.currentConnectionId) )
            {
                connection++;
            }
            this.previousConnectionId = this.currentConnectionId;
            
            // If this is the 10th test in a row, perform a bandwidth test
            // measurement to update our figures.
            if( testNumber % 10 == 0 )
            {
                bandwidth = fetchMegabyteResponse(); // milliseconds for 8,000,000 bits
            }
            
            // Delay to ensure we wait at least 5 seconds before the next round.
            while( Math.abs(System.currentTimeMillis() - startTimestamp) < 5000 )
            {
                try
                {
                    Thread.currentThread().join(Math.abs(System.currentTimeMillis() - startTimestamp)+1);
                }
                catch( InterruptedException e )
                {
                    // Ignore
                }
            }
            
            // Publish result
            this.results
                    .append(testNumber).append(',')
                    .append(format.format(now)).append(',')
                    .append(connection).append(',')
                    .append(pingTimeMillis).append(',')
                    .append(bandwidth).append('\n');
            this.publish(0);
            
            // Next test
            testNumber++;
        }
        
        // Return the answer
        try{ this.conn.close(); } catch( Exception f ) {/* ignore */}
        return this.results.toString();
    }
    
    public String getStatus() {
        return "Test " + this.testNumber + " - Connection " + this.connection + " (" + this.currentConnectionId + ")";
    }

    @Override
    protected void done() {
        String result = this.results.toString();
        this.callback.updateText(result);
        this.callback.finished();
    }

    @Override
    protected void process(List<Integer> chunks) {
        String result = this.results.toString();
        this.callback.updateText(result);
    }

    /**
     * Connect if we aren't already connected. Capture the connection ID if
     * we could connect, and record time in milliseconds to do so. Return
     * 0 if unable to connect.
     */
    private long fetchConnectionAndIdentifier()
    {
        String url = type.getUrl(host, database);
        
        if( this.conn == null )
        {
            try
            {
                this.conn = connect(url, username, password);
            }
            catch( Exception e )
            {
                try{ this.conn.close(); } catch( Exception f ) {/* ignore */}
                this.conn = null;
                return 0;
            }
        }
        
        String query = type.getConnectionIdQuery();
        
        try
        {
            long start = System.currentTimeMillis();
            String connectionId = queryResult(conn, query, 60);
            long duration = System.currentTimeMillis() - start;
            this.currentConnectionId = connectionId;
            return duration;
        }
        catch( Exception e )
        {
            try{ this.conn.close(); } catch( Exception f ) {/* ignore */}
            this.conn = null;
            return 0;
        }
    }
    
    // Measure time taken to fetch a Metabyte sized response, gives download bandwidth
    private long fetchMegabyteResponse()
    {
        if( this.conn == null ) return 0;
        
        try
        {
            String query = type.getQuery(1000000);
            long start = System.currentTimeMillis();
            query(conn, query, 60);
            long duration = System.currentTimeMillis() - start;
            return duration;
        }
        catch( Exception e )
        {
            try{ this.conn.close(); } catch( Exception f ) {/* ignore */}
            this.conn = null;
            return 0;
        }
    }
    
//    private void ping()
//    {
//        loadDrivers();
//        
//        String url = type.getUrl(host, database);
//        
//        /*
//            To test we first try and establish a connection. That gives us the connection time.
//            Can try creating and closing a connection several times to get an idea.
//        
//            Now with an already open connection try running "SELECT 1" queries.
//            This will measure round-trip time for minimal packet size.
//        
//        */
//        
//        Connection conn = null;
//        
//        // Try connecting and disconnecting 10 times in a row
//        for( int attempt = 1; attempt <= 10; attempt ++ )
//        {
//            String activity = "Connection";
//            try
//            {
//                final long start = System.currentTimeMillis();
//                conn = connect(url, username, password);
//                final long duration = System.currentTimeMillis() - start;
//                if( attempt < 10 )
//                    try{ conn.close(); } catch( Exception e ) {/* ignore */}
//                
//                results.add(new PingResult(activity, attempt, duration));
//            }
//            catch( SQLException e )
//            {
//                results.add(new PingResult(activity, attempt, e));
//            }
//        }
//        if( conn != null )
//        {
//            final int[] SIZES = new int[]{1,10000,100000,1000000};
//            for( int size = 0; size < SIZES.length; size++ )
//            {
//                String query = type.getQuery(SIZES[size]);
//                String activity = query; // NAMES[size];
//                for( int attempt = 1; attempt <= 10; attempt ++ )
//                {
//                    try
//                    {
//                        final long start = System.currentTimeMillis();
//                        query(conn, query, 60);
//                        final long duration = System.currentTimeMillis() - start;
//
//                        results.add(new PingResult(activity, attempt, duration));
//                    }
//                    catch( SQLException e )
//                    {
//                        results.add(new PingResult(activity, attempt, e));
//                    }
//                }
//            }
//        }
//        
//        // Close our shared connection
//        try{ conn.close(); } catch( Exception e ) {/* ignore */}
//    }
    
    private static Connection connect(String url, String username, String password) throws SQLException
    {
        Connection conn = DriverManager.getConnection(url, username, password);
        return conn;
    }
    
    private static void query(Connection conn, String query, int timeoutSeconds) throws SQLException
    {
        Statement stmt = null;
        ResultSet rs = null;
        try
        {
            stmt = conn.createStatement();
            stmt.setQueryTimeout(timeoutSeconds);
            rs = stmt.executeQuery(query);
            while( rs.next() )
            {
                // do nothing
            }
        }
        finally
        {
            try{ rs.close(); } catch( Exception e ) {/* ignore*/}
            try{ stmt.close(); } catch( Exception e ) {/* ignore*/}
        }
    }
    
    private static String queryResult(Connection conn, String query, int timeoutSeconds) throws SQLException
    {
        Statement stmt = null;
        ResultSet rs = null;
        try
        {
            stmt = conn.createStatement();
            stmt.setQueryTimeout(timeoutSeconds);
            rs = stmt.executeQuery(query);
            rs.next();
            return rs.getString(1);
        }
        finally
        {
            try{ rs.close(); } catch( Exception e ) {/* ignore*/}
            try{ stmt.close(); } catch( Exception e ) {/* ignore*/}
        }
    }
    
    private static void loadDrivers()
    {
        try
        {
            Class.forName("net.sourceforge.jtds.jdbc.Driver").newInstance();
            Class.forName("org.postgresql.Driver").newInstance();
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        }
        catch( ClassNotFoundException e )
        {
            throw new AssertionError(e);
        }
        catch( InstantiationException e )
        {
            throw new AssertionError(e);
        }
        catch( IllegalAccessException e )
        {
            throw new AssertionError(e);
        }
    }
}
