package org.bitbucket.gavannewell.databaseping;

/**
 *
 * @author Gavan Newell
 */
public enum DatabaseType {
    
    MySQL(
            "SELECT REPEAT('A', %)", 
            "jdbc:mysql://HOST:3306/DATABASE?connectTimeout=60000&socketTimeout=60000",
            "SELECT CONNECTION_ID()"),
    PostgreSQL(
            "SELECT REPEAT('A', %)", 
            "jdbc:postgresql://HOST:5432/DATABASE?connectTimeout=60&loginTimeout=60&socketTimeout=60",
            "SELECT pg_backend_pid()"),
    SQLServer(
            "SELECT REPLICATE(CAST('A' AS varchar(max)), %)", 
            "jdbc:jtds:sqlserver://HOST:1433/DATABASE;loginTimeout=60;socketTimeout=60",
            "SELECT @@SPID");
    
    private final String queryFormat; // substitute % with number
    private final String urlFormat; // substitute 'HOST' and 'DATABASE'
    private final String connectionIdQuery;  

    private DatabaseType(String queryFormat, String urlFormat, String connectionIdQuery) {
        this.queryFormat = queryFormat;
        this.urlFormat = urlFormat;
        this.connectionIdQuery = connectionIdQuery;
    }

    public String getQuery(int repeats) {
        return queryFormat.replace("%", Integer.toString(repeats));
    }

    public String getUrl(String host, String database) {
        return urlFormat.replace("HOST", host).replace("DATABASE", database);
    }

    public String getConnectionIdQuery() {
        return connectionIdQuery;
    }
}
