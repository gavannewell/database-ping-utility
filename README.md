# README #

Utility program to 'ping' a MySQL / PostgreSQL / SQL Server database. Measures the time to connect, execute a simple query ("SELECT 1") and execute queries that return approximately 10k, 100k and 1M of data.